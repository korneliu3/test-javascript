<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="computer11" />

	<title>S</title>
    
    
    <script type="text/javascript">
<!--
    function calculate_coin(){
	   // Get the value of input
       var get_input =  document.getElementById("input_data").value ;
       
       var two_pound =0;
       var one_pound =0;
       var fifty_pennies=0;
       var twenty_pennies =0;
       var two_pennies = 0;
       var one_penny = 0;
       var pennies=0;
       //
       /*
       
            REGEX:
        
            /: Delimiters of regex
            ^: Starts with
            \d: Any digit
            +: One or more of the preceding characters
            $: End
       */
           // remove the free space from number for examle if the people enter £2 . 23
            get_input = get_input.replace(" ","");
            get_input = get_input.replace(" ","");
            get_input = get_input.replace(" ","");
            get_input = get_input.replace(" ","");
            
       // check if the input data contain only digit numbers OR  digit numbers and letter "p"
       if(/^\d+$/.test(get_input) === true || /^\d+p+$/.test(get_input) === true){
            pennies = parseInt(get_input);
       }
      
       // check if input start with "£" OR check if the input is float
       if(get_input.startsWith("£") == true || !isNaN(get_input) && get_input.toString().indexOf('.') != -1  == true){
            // remove character pound £
            get_input = get_input.replace("£","");
            // remove character "p"
            get_input = get_input.replace("p","");
            
            // round number with two decimals no matter was, for example if is 2.332234 it will show 2.33 and convert the pound in pinnies
            pennies = parseFloat(get_input).toFixed(2) * 100;
            // Round number because give not right number example from this 2342.20 * 100 =  234219.99999999997---
              pennies = Math.round( pennies )  ;
         }
       
      
         // Start The process of calculating numbers of couin
          
         //  check if is the imput data is biggar then 200 
        if(pennies >= 200){
          //get how many couin by 2 pounds are in number  
         two_pound = parseInt(pennies/200);
          // take the int number from total penies  
         pennies = pennies - (200 * two_pound);
        } 
        
        if(pennies >= 100){
           //get how many couin by 1 pounds are in number 
          one_pound = parseInt(pennies/100);
          pennies = pennies - (100 * one_pound );
         }
       
       if(pennies >= 50){
        //get how many couin by 50p are in number 
          fifty_pennies = parseInt(pennies/50);
          pennies = pennies - (50 * fifty_pennies );
         }
         
          if(pennies >= 20){
            //get how many couin by 20p are in number 
           twenty_pennies = parseInt(pennies/20);
           pennies = pennies - (20 * twenty_pennies );
         }
         
          if(pennies >= 2){
            //get how many couin by 2p are in number 
           two_pennies = parseInt(pennies/2);
           pennies = pennies - (2 * two_pennies );
         }
         
         if(pennies >= 1){
            //get how many couin by 1p are in number 
           one_penny = parseInt(pennies/1);
           pennies = pennies - (1 * one_penny );
         }
         
        document.getElementById('show_result').innerHTML =  two_pound + " coin by £2 <br/> " +one_pound  +" coin by £1 <br/> "+fifty_pennies  +" coin by 50p <br/> "+ twenty_pennies  +" coin by 20p <br/> "+two_pennies  +" coin by 2p <br/> "+one_penny  +" coin by 1p";
       return false;
	}
-->
</script>

<style type="text/css">
<!--
/*use this when use inside block floate left*/
    html {
    	font-family: sans-serif;
    	line-height: 1.15;
    	-ms-text-size-adjust: 100%;
    	-webkit-text-size-adjust: 100%;
    }
    * {
      box-sizing: border-box;
    }
    .clear-selection{
        clear: both;
    }
    
    /*add background image to the website */
    body {
        width: 100%;
        min-height: 100%;
    	margin: 0;
     }

	.form{
	   width: 350px;
       margin: 50px auto;
	}
    .form input{
	   width: 70%;
       float: left;
       padding: 5px 10px;
       border: 1px solid #ccc;
	}
    .form button{
	   width: 30%;
       float: left;
       text-align: center;
       padding: 5px 10px;
       border: 1px solid #ccc;
       background: #ccc;
       color: #000;
	}
    
-->
</style>
</head>

<body>

<div class="form">


<input type="text" value="" id="input_data" />
<button onclick="return calculate_coin();">Calculate</button>
<div class="clear-selection"></div>
<div id="show_result"></div>

</div>
</body>
</html>